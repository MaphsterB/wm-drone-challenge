# Drone Delivery Challenge

Solution for WM Drone Delivery Challenge, implemented in Python.

## Overview

This project implements a simple single-drone algorithm for scheduling deliveries to
customers in a small town. The objective of the scheduling algorithm is to maximize
**Net Promoter Score** (NPS) - a measure of the number of deliveries made in under
2 hours, minus the number of deliveries made in 4 hours or more, expressed as a percentage
of total orders.

Please see the assignment PDF for full details and requirements, including input and output
file formats.

All program operation is from the command-line. To view the program manual, use:

`python wmdc.py --help`

For normal operation, provide input and output file paths:

`python wmdc.py -i <input-file> -o <output-file>`

Upon successful execution, the output file will be created or overwritten if it already exists,
and the program exit code will be 0.

Non-zero exit codes are as follows:

* 1: Could not open input file
* 2: Incorrect input file format
* 3: Could not open output file

Note that unschedulable deliveries will not result in a non-zero exit code. See "Assumptions" below.

## License

This work is disclaimed of all copyright by way of The Unlicense. Use it in any way you wish.

https://unlicense.org/

## Assumptions

The following assumptions were made when designing the program. Some are simplifying assumptions
for the purpose of keeping the prototype code easy to read. Others make decisions related to gaps
in the specification (we treat all gaps in the specification as "implementation-defined," meaning
that we are allowed to do whatever we want, as long as it is documented and consistent).

* The input file format may not be correct. If incorrect input is discovered, the program should
  halt with a non-zero exit code.
* We assume strict input processing. No empty lines, etc.
* In the input file, the N/S direction indicator will always precede the E/W direction indicator.
  If necessary, the regex used to parse input could be extended to allow either ordering.
* All coordinates will be non-negative integers (e.g. "N-7" is invalid; it does not equal "S7.")
* Either coordinate may be zero, but the special case (0, 0) is forbidden.
* All order timestamps are for the same 24-hour period, in local time.
* Daylight Savings Time does not need to be accounted for.
* The drone takes zero time for everything except travel (e.g. dropping off and picking up packages).
  To relax this assumption, we could simply add more time when calculating delivery and/or return times
  in the Delivery class.
* As a simplifying assumption, the drone may only travel horizontally and vertically (not diagonally).
  This could be rationalized as the drone not being allowed to fly over customers' yards. To relax this
  assumption, the distance computation could be extended to use the Pythagorean Theorem rather than
  simply summing the coordinates.
* Because the drone is only allowed to operate from 6AM to 10PM, it must not be in mid-delivery
  at 10PM.
* Orders could arrive at any time of day, including outside Drone operating hours.
* Time to delivery is irrespective of drone operating hours. If an order is placed at midnight, and the
  drone cannot depart until 6AM, for example, then that order will be a detractor.
* Not all orders will be deliverable. These include orders placed on or after 10PM. Because no
  specification was given for how to handle such orders, the output file will have departure time
  specified as "undeliverable." The NPS computation will treat the ratings for such orders as detractors.
* We assume that the drone cannot depart to deliver an order before that order's timestamp. This
  makes the optimization algorithm more of a "retrospective" - it would not make sense to accumulate
  all of the orders for a day and *then* schedule them all, because then nothing would be delivered.
  Chalk it up to this being a homework problem rather than production software ;)
* We assume that the NPS of an empty list of deliveries is undefined. The choice for how to handle it
  is to pass it silently, make up a value, or treat it as an error condition. We choose to warn
  the user via a message on STDERR, but otherwise the program terminates normally, and does not
  generate an output file.
* Finally, we assume NPS in the output format should be an integer percentage, to match the
  example output file shown. Fractional percentages should be rounded to the nearest whole
  number.

## Design

The program consists of 4 modules: the main module and 3 modules containing supporting classes.
The main module contains all input/output code, including CLI argument parsing, input file parsing,
and output file generation. The support classes are as follows:

* `Order` - Immutable data object representing an order. Stores the order's ID, coordinates, and
  timestamp, and computes delivery distance from the coordinates. Has a `fromstr()` helper method
  that generates an Order from a single line of the input file, per the specification.

* `Delivery` - Mutable object representing a delivery. Each delivery has an Order, and possibly
  a scheduled departure time. When departure time is set, the Delivery uses it and the order distance
  to compute the delivery and return times of the drone, the estimated customer rating, and the
  rating category (promoter, neutral, or detractor). We encode these categories as the integers
  (1, 0, -1) respectively, as it simplifies the NPS computation.

* `Scheduler` - Class containing the scheduling algorithm. Given a list of Orders as inputs,
  a Scheduler will generate a list of Delivery objects. Calling the `maximize_nps()` method
  will result in the Scheduler re-ordering Deliveries for maximum promoter score. The NPS can
  then be retrieved from the Scheduler's `get_nps()` method.

## Performance

We assume an approximate upper bound on the size of the order list based on a single day's orders.
Pessimistically, if the same customer, situated immediately next to the warehouse, placed enough
orders to keep the drone operating continuously throughout the day, the drone could deliver them
at a rate of 1 every 2 minutes (based on the shortest possible travel distance of 1.)

If the drone could operate 24 hours a day, it could make a maximum of `24*60 / 2 = 720` deliveries
per day. Given restrictions of 6AM - 10PM, that maximum becomes `(22-6+1)*60 / 2 = 510` deliveries.

While it is possible for more orders to be placed than that, and the drone simply couldn't deliver
them all, the priority in that case would most likely be buying more drones, and a modified algorithm
would be needed to accommodate multiple drones (or multiple copies of this program in parallel).
Thus, in the interests of avoiding premature optimization, polynomial time execution should be good
enough for on the order of 500 - 1,000 orders per day.

## Contributing

Running the program requires a Python 3.7 environment, with no other dependencies. Developers,
however, should install the following dependencies:

* `pylint`
* `pytest`
* `pytest-cov`
* `pytest-mock`

PyLint and PyTest are used for code quality checking and for automated unit testing on this project.
GitLab CI jobs are configured to automate this process on each push to master/develop.

All dependencies can be installed in a Python virtualenv using the following commands from the project root:

```sh
python -m venv venv
venv/bin/pip install -r requirements-dev.txt
venv/bin/pip install -e .
```

To lint the code:

`pylint wmdc.py _wmdc tests`

To run automated tests and generate coverage reports:

`pytest -v --cov --cov-report term-missing`

These are the recommended commands; for full details consult the pylint & pytest docs.
