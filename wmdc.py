"""
wm-drone-scheduler.py - Main script for WM Drone Delivery Challenge.
    See usage and README for details.
"""


import argparse
import enum
import sys

from typing import Iterator, Sequence, TextIO

from _wmdc import Delivery, Order, Scheduler


# Global config vars. In production, we would move these to a
# configuration file, and use a ConfigParser to read them.
# For prototyping, read-only global state is fine.
OPENING_HOUR = 6
CLOSING_HOUR = 22


class ExitCodes(enum.Enum):
    """
    Program exit code enumeration.
    """
    Okay = 0
    ErrorOpeningInput = 1
    BadInputFileFormat = 2
    ErrorOpeningOutput = 3


def main() -> ExitCodes:
    """
    Program entry point.
    Parse CLI args, parse input file, perform scheduling, generate output.
    """
    # CLI arg parsing.
    parser = argparse.ArgumentParser(description="Drone delivery scheduler.")
    parser.add_argument(
        "--input-file", "-i",
        type=str,
        default="-",
        help="Input file path; '-' for STDIN (default).",
    )
    parser.add_argument(
        "--output-file", "-o",
        type=str,
        default="-",
        help="Output file path; '-' for STDOUT (default).",
    )
    args = parser.parse_args()

    # Parse input file; get list of Orders.
    try:
        if args.input_file == '-':
            orders = list(parse_orders()) # pragma: no cover
        else:
            with open(args.input_file) as file:
                orders = list(parse_orders(file=file))
    except OSError as ex:
        print(f"Could not open input file '{args.input_file}.' Error: {ex}", file=sys.stderr)
        return ExitCodes.ErrorOpeningInput
    except ValueError as ex:
        print(ex, file=sys.stderr)
        return ExitCodes.BadInputFileFormat

    # If no orders to deliver: exit early, but normally.
    if not orders:
        print("No deliveries to schedule. Exiting.", file=sys.stderr)
        return ExitCodes.Okay

    # Schedule Orders for delivery.
    scheduler = Scheduler(
        orders,
        min_departure=OPENING_HOUR * 3600,
        max_return=CLOSING_HOUR * 3600,
    )
    scheduler.maximize_nps()

    # Generate output file.
    try:
        if args.output_file == '-':
            generate_output(scheduler.deliveries, scheduler.get_nps()) # pragma: no cover
        else:
            with open(args.output_file, "w") as file:
                generate_output(scheduler.deliveries, scheduler.get_nps(), file=file)
    except OSError as ex:
        print(f"Could not open output file '{args.output_file}.' Error: {ex}", file=sys.stderr)
        return ExitCodes.ErrorOpeningOutput

    return ExitCodes.Okay


def parse_orders(file: TextIO = sys.stdin) -> Iterator[Order]:
    """
    Parse input file, as generator function (yield 1 Order per input line)
    Raise ValueError if any problems are discovered.
    """
    ids = set()
    prev_time = None
    for (linenum, line) in enumerate(file.readlines()):
        try:
            order = Order.fromstr(line)
            if order.id in ids:
                raise ValueError(f"ID {order.id} is not unique.")
            ids.add(order.id)
            if prev_time and order.time < prev_time:
                raise ValueError(f"Orders are not sorted by timestamp.")
            prev_time = order.time
            yield order
        except ValueError as ex:
            # Re-raise w/ line number appended.
            raise ValueError(f"Error on line {linenum + 1}: {ex}")


def generate_output(
        deliveries: Sequence[Delivery],
        nps: float,
        file: TextIO = sys.stdout) -> None:
    """
    Write output file, as one line per Delivery, then the NPS at the bottom.
    """
    for delivery in deliveries:
        print(str(delivery), file=file)
    print(f"NPS {int(round(nps))}", end="", file=file)


if __name__ == "__main__": # pragma: no cover
    exit(main().value)
