"""
tests.py - Unit test suite for WM Drone Delivery Challenge.
    To run: pytest tests.py --cov --cov-report term-missing
    (omit --cov arguments to skip coverage reporting)

    Note: If you insert breakpoint()'s to enter pdb during debugging
    when mock_open()'s are enabled, it will cause weird SyntaxErrors
    when starting the debugger. These are due to the fact that pdb
    looks for a few .pdb-like config files when starting up, and can
    safely be ignored (unless you need special pdb config).
"""
# pylint: disable=redefined-outer-name,unused-argument


import builtins
import sys

import pytest

import wmdc
from wmdc import ExitCodes
from _wmdc import Delivery, Order, Scheduler


@pytest.fixture
def create_mock_input_file(mocker):
    """
    Factory fixture for a fake input file.
    Creates a mock file handle and mocks the Python
    open() builtin to return it.
    """
    default_lines = (
        "WM001 N1E1 00:00:00",
        "WM002 N1E1 00:00:00",
        "WM003 N1E1 00:00:00",
    )
    def inner(lines=None):
        if lines is None:
            lines = default_lines
        mock = mocker.mock_open(read_data="\n".join(lines))
        mocker.patch.object(builtins, "open", mock)
        return mock
    return inner


@pytest.fixture
def mock_scheduler(mocker):
    """
    Mock the Scheduler functionality for entry point testing.
    """
    mock = mocker.Mock(spec=Scheduler([]))
    mock.deliveries = [
        Delivery(Order("WM001", ("N1", "E1"), 0), 0),
        Delivery(Order("WM002", ("N1", "E1"), 0), 12*3600),
    ]
    mock.nps = mocker.Mock(return_value=42)
    mock_cls = mocker.Mock(spec=Scheduler, return_value=mock)
    mocker.patch.object(wmdc, "Scheduler", mock_cls)
    return mock


class TestEntryPoint:
    """
    Tests of the main entry point and its helpers.
    """

    @staticmethod
    def test_parse_orders(create_mock_input_file):
        """
        Ordinary test of parse_orders() helper.
        """
        mock_open = create_mock_input_file()
        with mock_open() as file:
            ret = list(wmdc.parse_orders(file))
        assert all(int(o.id[-1]) - 1 == i for (i, o) in enumerate(ret))
        assert all(o.dist == 2 for o in ret)
        assert all(o.time == 0 for o in ret)

    @staticmethod
    def test_parse_orders_duplicate_ids(create_mock_input_file):
        """
        Verify that order id's are checked for uniqueness.
        """
        lines = (
            "WM001 N1E1 00:00:00",
            "WM001 N1E1 00:00:00",
        )
        mock_open = create_mock_input_file(lines)
        with pytest.raises(ValueError) as einfo:
            with mock_open() as file:
                _ = list(wmdc.parse_orders(file))
        assert "Error on line 2: ID WM001 is not unique." in str(einfo.value)

    @staticmethod
    def test_parse_orders_not_in_timestamp_order(create_mock_input_file):
        """
        Verify that order id's are checked for uniqueness.
        """
        # pylint: disable=invalid-name
        lines = (
            "WM001 N1E1 00:00:01",
            "WM002 N1E1 00:00:00",
        )
        mock_open = create_mock_input_file(lines)
        with pytest.raises(ValueError) as einfo:
            with mock_open() as file:
                _ = list(wmdc.parse_orders(file))
        assert "Error on line 2: Orders are not sorted by timestamp." in str(einfo.value)

    @staticmethod
    def test_generate_output(mocker):
        """
        Verify generate_output() ... er ... generates output.
        """
        deliveries = [
            Delivery(Order("WM001", ("N1", "E1"), 0), 0),
            Delivery(Order("WM002", ("N1", "E1"), 0), 12*3600),
        ]
        mock_file = mocker.mock_open()()
        wmdc.generate_output(deliveries, 42, mock_file)
        # Each call to print() generates 2 write() calls on the file handle;
        # the 1st is our string, the 2nd is a newline (or empty string for
        # the last line.)
        correct_output = [
            "WM001 00:00:00", "\n",
            "WM002 12:00:00", "\n",
            "NPS 42", ""
        ]
        assert mock_file.write.call_count == len(correct_output)
        for (i, correct) in enumerate(correct_output):
            assert mock_file.write.call_args_list[i][0][0] == correct

    @staticmethod
    def test_main(mocker, create_mock_input_file, mock_scheduler):
        """
        Test normal execution of the main entry point.
        We won't concern ourselves with testing the argparser; it's
        standard, built-in, and non-side-effecting. So rather than
        mock it, we'll just set sys.argv directly.
        """
        sys.argv = ["pytest", "-i", "bogus_input", "-o", "bogus_output"]
        mock_parse_orders = mocker.patch.object(
            wmdc,
            "parse_orders",
            autospec=True,
            return_value=[Order("WM001", ("N1", "E1"), 0)]
        )
        mock_generate_output = mocker.patch.object(wmdc, "generate_output", autospec=True)
        mock_open = create_mock_input_file()
        ret = wmdc.main()
        assert mock_open.call_count == 2
        assert mock_open.call_args_list[0][0][0] == "bogus_input"
        assert mock_parse_orders.call_count == 1
        assert mock_scheduler.maximize_nps.call_count == 1
        assert mock_open.call_args_list[1][0][0] == "bogus_output"
        assert mock_generate_output.call_count == 1
        assert ret == ExitCodes.Okay

    @staticmethod
    def test_main_os_error_on_input(capsys, mocker):
        """
        Verify that main() gracefully handles OSErrors when trying to open
        the input file.
        """
        sys.argv = ["pytest", "-i", "bogus_input", "-o", "bogus_output"]
        def raiser(file, mode="r"): raise OSError("Test Error") # pylint: disable=multiple-statements
        mocker.patch.object(builtins, "open", side_effect=raiser)
        ret = wmdc.main()
        captured = capsys.readouterr()
        assert "Could not open input file 'bogus_input.' Error: Test Error" in captured.err
        assert ret == ExitCodes.ErrorOpeningInput

    @staticmethod
    def test_main_value_error_on_input(capsys, mocker, create_mock_input_file):
        """
        Verify that main() handles ValueErrors from parse_orders() correctly.
        (Note: We already verified individual ValueErrors while parsing in the parse_orders
        unit tests above. Here, we are only verifying main()'s handling of the error.)
        """
        _ = create_mock_input_file()
        def raiser(file=None): raise ValueError("Test Error") # pylint: disable=multiple-statements
        mocker.patch.object(wmdc, "parse_orders", autospec=True, side_effect=raiser)
        ret = wmdc.main()
        captured = capsys.readouterr()
        assert captured.err == "Test Error\n"
        assert ret == ExitCodes.BadInputFileFormat

    @staticmethod
    def test_main_no_orders(capsys, mocker):
        """
        Verify that no errors occur when an empty input file is provided.
        (Note: mock_open() by default provides an empty file when read.)
        """
        sys.argv = ["pytest", "-i", "bogus_input", "-o", "bogus_output"]
        mocker.patch.object(builtins, "open", mocker.mock_open())
        ret = wmdc.main()
        captured = capsys.readouterr()
        assert "No deliveries to schedule" in captured.err
        assert ret == ExitCodes.Okay

    @staticmethod
    def test_main_os_error_on_output(capsys, mocker):
        """
        Verify that main() gracefully handles OSErrors when trying to open
        the output file.
        """
        sys.argv = ["pytest", "-i", "bogus_input", "-o", "bogus_output"]
        def raiser(file, mode="r"):
            if file == "bogus_output":
                raise OSError("Test Error")
            return mocker.DEFAULT
        mocker.patch.object(builtins, "open", side_effect=raiser)
        mocker.patch.object(
            wmdc,
            "parse_orders",
            autospec=True,
            return_value=[Order("WM001", ("N1", "E1"), 0)]
        )
        ret = wmdc.main()
        captured = capsys.readouterr()
        assert "Could not open output file 'bogus_output.' Error: Test Error" in captured.err
        assert ret == ExitCodes.ErrorOpeningOutput
