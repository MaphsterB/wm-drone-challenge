"""
test_scheduler.py - Test module for the Scheduler class.
"""
# pylint: disable=invalid-name


from wmdc import Order, Scheduler


class TestScheduler:
    """
    Tests of the scheduling algorithm class.
    """

    @staticmethod
    def test_constructor():
        """
        Basic constructor coverage tests.
        Empty list, single order, multiple orders.
        Default minimum departure, set minimum departure.
        """
        sched1 = Scheduler([])
        assert sched1.orders == []
        assert sched1.deliveries == []
        assert sched1.min_departure == 0
        sched2 = Scheduler([Order("WM001", ("N5", "E5"), 0)])
        assert len(sched2.orders) == 1
        assert len(sched2.deliveries) == 1
        assert sched2.min_departure == 0
        sched3 = Scheduler([
            Order("WM001", ("N5", "E5"), 1*3600),
            Order("WM002", ("N5", "E5"), 1*3600),
            Order("WM003", ("N5", "E5"), 2*3600),
        ], min_departure=3700)
        assert len(sched3.orders) == 3
        assert len(sched3.deliveries) == 3
        assert sched3.min_departure == 3700
        assert sched3.deliveries[0].dep_time == 3700
        assert sched3.deliveries[1].dep_time == 3700 + 20*60
        assert sched3.deliveries[2].dep_time == 2*3600

    @staticmethod
    def test_set_departures_start_end():
        """
        Test set_departures() with start/end specified.
        """
        orders = [
            Order("WM001", ("N5", "E5"), 12*3600),
            Order("WM002", ("N5", "E5"), 12*3600),
            Order("WM003", ("N5", "E5"), 12*3600),
        ]
        sched = Scheduler(orders)
        # sched already called set_departures() normally.
        # Crack open its delivery list and set dep_times back to
        # None for this test.
        for d in sched.deliveries:
            d.dep_time = None
        sched.set_departures(start=1, end=2)
        assert sched.deliveries[0].dep_time is None
        assert sched.deliveries[1].dep_time == 12 * 3600
        assert sched.deliveries[2].dep_time is None

    @staticmethod
    def test_set_departures_min_departure_time():
        """
        Test set_departures() with a minimum departure time set.
        """
        orders = [
            Order("WM001", ("N5", "E5"), 0),
            Order("WM002", ("N5", "E5"), 1*3600),
            Order("WM003", ("N5", "E5"), 2*3600),
        ]
        sched = Scheduler(orders, min_departure=4*3600)
        assert sched.deliveries[0].dep_time == 4*3600
        assert sched.deliveries[1].dep_time == 4*3600 + 20*60
        assert sched.deliveries[2].dep_time == 4*3600 + 40*60

    @staticmethod
    def test_set_departures_max_return_time():
        """
        Test set_departures() with a maximum return time set.
        """
        orders = [
            Order("WM001", ("N5", "E5"), 21*3600),
            Order("WM002", ("N5", "E5"), 22*3600 - 20*60),
            Order("WM003", ("N5", "E5"), 22*3600 - 19*60),
        ]
        sched = Scheduler(orders, max_return=22*3600)
        assert sched.deliveries[0].dep_time == 21*3600
        assert sched.deliveries[1].dep_time == 22*3600 - 20*60
        assert sched.deliveries[2].dep_time is None

    @staticmethod
    def test_get_nps():
        """
        Test NPS computation specifically, because maximize_nps() only
        uses the nps_num().
        """
        promo = Order("WM001", ("N1", "E1"), 0)
        neut = Order("WM002", ("N60", "E60"), 6*3600)
        det = Order("WM003", ("N120", "E120"), 12*3600)
        assert Scheduler([promo]).get_nps() == 100
        assert Scheduler([neut]).get_nps() == 0
        assert Scheduler([det]).get_nps() == -100
        assert Scheduler([promo, promo, promo]).get_nps() == 100
        assert Scheduler([promo, neut, det]).get_nps() == 0

    @staticmethod
    def test_maximize_empty_list():
        """
        maximize_nps() should not crash when given an empty list.
        """
        scheduler = Scheduler([])
        scheduler.maximize_nps()
        assert scheduler.deliveries == []

    @staticmethod
    def test_maximize_single_order():
        """
        maximize_nps() with a single order should be equivalent to a no-op.
        """
        orders = [Order("WM001", ("N5", "E5"), 12*3600)]
        scheduler = Scheduler(orders)
        scheduler.maximize_nps()
        assert scheduler.orders == orders
        assert len(scheduler.deliveries) == 1
        assert scheduler.deliveries[0].order == orders[0]

    @staticmethod
    def verify_maximize(orders, min_departure=6*3600, max_return=22*3600):
        """
        Test helper: given a list of orders, use slow_maxmize_nps()
        to determine the correct maximum, then compare its output to
        that of our scheduling algorithm.
        """
        sched = Scheduler(orders, min_departure=min_departure, max_return=max_return)
        (deliveries, correct) = Scheduler.slow_maximize_nps(orders, min_departure, max_return)
        sched.maximize_nps()
        nps = sched.get_nps()
        assert nps == correct
        return (sched, deliveries, nps)

    def test_maximize_3_promoters_different_times(self):
        """
        If the deliveries are all promoters, no reordering should occur.
        This test has them with different timestamps.
        """
        orders = [
            Order("WM001", ("N5", "E5"), 12*3600),
            Order("WM002", ("N5", "E5"), 13*3600),
            Order("WM003", ("N5", "E5"), 14*3600),
        ]
        (sched, correct, _) = self.verify_maximize(orders)
        assert sched.deliveries == list(correct)

    def test_maximize_3_promoters_same_times(self):
        """
        If the deliveries are all promoters, no reordering should occur.
        This test has them with the same timestamp.
        """
        orders = [
            Order("WM001", ("N5", "E5"), 12*3600),
            Order("WM002", ("N5", "E5"), 12*3600),
            Order("WM003", ("N5", "E5"), 12*3600),
        ]
        (sched, correct, _) = self.verify_maximize(orders)
        assert sched.deliveries == list(correct)

    def test_maximize_long_delivery_to_end_same_times(self):
        """
        Test that a long delivery is successfully delayed to increase
        other orders to promoters when all orders have the same timestamp.
        [D D D] -> [P P D]
        """
        orders = [
            Order("WM001", ("N60", "E60"), 12*3600),
            Order("WM002", ("N5", "E5"), 12*3600),
            Order("WM003", ("N5", "E5"), 12*3600),
        ]
        _ = self.verify_maximize(orders)

    def test_maximize_long_delivery_with_neutral_same_times(self):
        """
        Test that a long delivery is successfully delayed to increase
        other orders to promoters when all orders have the same timestamp.
        This time, one order can only be made neutral.
        [D D D] -> [P N D]
        """
        orders = [
            Order("WM001", ("N60", "E60"), 12*3600),
            Order("WM002", ("N50", "E9"), 12*3600),
            Order("WM003", ("N1", "E1"), 12*3600),
        ]
        _ = self.verify_maximize(orders)

    def test_maximize_with_all_detractors(self):
        """
        Test that maximizing when all orders are before opening time
        still functions correctly (it should just schedule all orders
        as-is, but when all 3 orders must be detractors, order order
        is actually irrelevant).
        """
        orders = [
            Order("WM001", ("N5", "E5"), 0),
            Order("WM002", ("N5", "E5"), 1*3600),
            Order("WM003", ("N5", "E5"), 2*3600),
        ]
        (_, _, nps) = self.verify_maximize(orders)
        assert nps == -100

    def test_maximize_after_closing(self):
        """
        Test that maximizing when all orders are after closing time
        still functions correctly (it should just schedule all orders
        as-is, but when all 3 orders must be detractors, order order
        is actually irrelevant).
        """
        orders = [
            Order("WM001", ("N5", "E5"), 22*3600),
            Order("WM002", ("N5", "E5"), 22*3600),
            Order("WM003", ("N5", "E5"), 22*3600),
        ]
        (_, _, nps) = self.verify_maximize(orders)
        assert nps == -100
