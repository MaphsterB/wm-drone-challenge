"""
test_order.py - Test module for the Order class.
"""


import pytest

from wmdc import Order


class TestOrderClass:
    """
    Unit testing for the Order class.
    """

    @staticmethod
    def test_constructor():
        """
        Basic test of constructor functionality.
        """
        order = Order("WM001", ("N1", "E2"), 60)
        assert order.id == "WM001"
        assert order.coords == ("N1", "E2")
        assert order.dist == 3
        assert order.time == 60

    @staticmethod
    def test_constructor_zero_distance():
        """
        Verify that ValueError is raised if a zero-distance Order
        is created.
        """
        with pytest.raises(ValueError) as einfo:
            _ = Order("WM001", ("N0", "E0"), 0)
        assert "Order distance must be non-zero" in str(einfo.value)

    @staticmethod
    def test_fromstr():
        """
        Ordinary test of Order.fromstr(). No error conditions.
        Exercise the regex with N/S/E/W directions.
        """
        order1 = Order.fromstr("WM001 N1E2 01:02:03\n")
        assert order1.id == "WM001"
        assert order1.coords == ("N1", "E2")
        assert order1.dist == 3
        assert order1.time == 3600 + 120 + 3
        order2 = Order.fromstr("WM001 S1W2 01:02:03")
        assert order2.coords == ("S1", "W2")

    @staticmethod
    def test_fromstr_validation_errors():
        """
        Test failures of each regexp used in fromstr().
        """
        bad_strings = [
            "WM01 N1E1 00:00:00",
            "WM001 N1N1 00:00:00",
            "WM001 N1E1 00:00",
        ]
        for ostr in bad_strings:
            with pytest.raises(ValueError) as einfo:
                _ = Order.fromstr(ostr)
            assert f"{ostr} does not match pattern" in str(einfo.value)

    @staticmethod
    def test_fromstr_time_validation_errors():
        """
        After the regex validation, fromstr() can still not be
        a valid time format. Verify.
        """
        # pylint: disable=invalid-name
        with pytest.raises(ValueError) as einfo:
            _ = Order.fromstr("WM001 N1E1 99:99:99")
        assert "99:99:99 does not match format" in str(einfo.value)

    @staticmethod
    def test_equality():
        """
        Test the __eq__() operator method.
        """
        # pylint: disable=comparison-with-itself
        # That's the point here!
        o1 = Order("WM001", ("N1", "E0"), 0)
        o2 = Order("WM002", ("N1", "E0"), 0)
        o3 = Order("WM001", ("N1", "E1"), 12*3600)
        assert o1 == o1
        assert o1 != o2
        assert o1 == o3

    @staticmethod
    def test_timestr():
        """
        Test Order.timestr() with a few values.
        """
        assert Order.timestr(0) == "00:00:00"
        assert Order.timestr(3600) == "01:00:00"
        assert Order.timestr(3600 + 6*60) == "01:06:00"
        assert Order.timestr(3600 + 6*60 + 30) == "01:06:30"
        assert Order.timestr(13*3600) == "13:00:00"
        assert Order.timestr(3600 + 6*60 + 30, fmt="%H:%M") == "01:06"

    @staticmethod
    def test_repr():
        """
        Basic test of __repr__(). Since this is a debugging tool,
        this test is optional.
        """
        ostr = repr(Order("WM001", ("N1", "E0"), 0))
        assert ostr == "Order('WM001', ('N1', 'E0'), 0)"

    @staticmethod
    def test_str():
        """
        Basic test of __str__().
        """
        ostr = str(Order("WM001", ("N1", "E0"), 0))
        assert ostr == "WM001 N1E0 00:00:00"
