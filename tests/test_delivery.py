"""
test_delivery.py - Test module for the Delivery class.
"""
# pylint: disable=invalid-name


import pytest

from wmdc import Order, Delivery


class TestDeliveryClass:
    """
    Unit test suite for the Delivery class.
    (Technically, integration testing with Order, since
    we do not mock out the dependency.) This should be
    fine, given Order's simplicity and the fact that
    Order itself is unit-tested.
    """

    @staticmethod
    def test_constructor_and_accessors_no_departure():
        """
        Test of the constructor and accessors.
        Verify that everything works with no departure set.
        """
        order = Order("WM001", ("N5", "E5"), 3600)
        delivery = Delivery(order)
        assert delivery.order is order
        assert delivery.dep_time is None
        assert delivery.del_time is None
        assert delivery.ret_time is None
        assert delivery.rating == 0
        assert delivery.category == Delivery.CATEGORIES[0]

    @staticmethod
    def test_constructor_and_accessors_with_departure():
        """
        Test of the constructor and accessors.
        Verify that everything works with a valid departure time set.
        """
        order = Order("WM001", ("N5", "E5"), 3600)
        delivery = Delivery(order, departure=2*3600)
        assert delivery.order is order
        assert delivery.dep_time == 2*3600
        assert delivery.del_time == 2*3600 + 10*60
        assert delivery.ret_time == 2*3600 + 20*60
        assert delivery.rating == 9
        assert delivery.category == Delivery.CATEGORIES[9]

    @staticmethod
    def test_constructor_with_assertion_error():
        """
        Delivery departure time must be at least the order's timestamp.
        As an internal interface, this is dealt with as an asserted
        precondition of setting departure time.
        """
        order = Order("WM001", ("N5", "E5"), 3600)
        # This not raising also checks the fencepost.
        _ = Delivery(order, departure=3600)
        with pytest.raises(AssertionError) as einfo:
            _ = Delivery(order, departure=3599)
        assert "Cannot depart before order timestamp" in str(einfo.value)

    @staticmethod
    def test_equality():
        """
        Test the __eq__() operator method for Delivery objects.
        """
        # pylint: disable=comparison-with-itself
        # That's the point here!
        o1 = Order("WM001", ("N1", "E0"), 0)
        o2 = Order("WM002", ("N1", "E0"), 0)
        o3 = Order("WM001", ("N1", "E1"), 12*3600)
        assert Delivery(o1) == Delivery(o1)
        assert Delivery(o1) != Delivery(o2)
        assert Delivery(o1) == Delivery(o3, departure=12*3600)

    @staticmethod
    def test_repr():
        """
        Basic test of repr() for Deliveries, with and without departure times.
        """
        order = Order("WM001", ("N5", "E5"), 3600)
        delivery1 = Delivery(order)
        delivery2 = Delivery(order, departure=2*3600)
        assert repr(delivery1) == "Delivery(Order('WM001', ('N5', 'E5'), 3600), departure=None)"
        assert repr(delivery2) == "Delivery(Order('WM001', ('N5', 'E5'), 3600), departure=7200)"

    @staticmethod
    def test_str():
        """
        Basic test of str() for Deliveries, with and without departure times.
        """
        order = Order("WM001", ("N5", "E5"), 3600)
        delivery1 = Delivery(order)
        delivery2 = Delivery(order, departure=2*3600)
        assert str(delivery1) == "WM001 undelivered"
        assert str(delivery2) == "WM001 02:00:00"
