"""
delivery.py - Module containing Delivery class.
"""


import enum

from typing import Optional

from .order import Order


class RatingCategory(enum.Enum):
    """
    Enumeration for rating categories: promoter, neutral, detractor.
    These are set up as integers 1, 0, -1 such that summing them is
    useful in computing NPS.
    """
    Promoter = 1
    Neutral = 0
    Detractor = -1


class Delivery:
    """
    Utility class representing a drone delivery. Stores the corresponding Order,
    and the departure time for the drone. When departure time is updated,
    the Delivery object automatically re-computes delivery/return times, and
    estimated customer rating and rating category (promoter/neutral/detractor).
    """

    # Class config var: list indexing rating category by rating.
    CATEGORIES = (
        7 * [RatingCategory.Detractor]
        + 2 * [RatingCategory.Neutral]
        + 2 * [RatingCategory.Promoter]
    ) # type: List[RatingCategory]

    def __init__(self, order: Order, departure: Optional[int] = None) -> None:
        self._order = order
        self._dtime = self._vtime = self._rtime = None
        self._rating = 0
        self._category = self.CATEGORIES[self._rating]
        self.dep_time = departure

    @property
    def order(self) -> Order:
        """
        Order is read-only once constructed; internal interfaces do not need
        to set it, and making it private means not dealing with having to
        recompute everything if the order or the dep_time changes.
        """
        return self._order

    @property
    def dep_time(self) -> int:
        """
        Delivery departure time. When set, also updates
        delivery and return times, rating (0-10), and rating category
        (promoter, neutral, detractor) as (+1, 0, -1) respectively.
        """
        return self._dtime

    @dep_time.setter
    def dep_time(self, val: int) -> None:
        if val is None:
            self._dtime = self._vtime = self._rtime = None
            self._rating = 0
        else:
            assert val >= self.order.time, "Cannot depart before order timestamp."
            delta_seconds = 60 * self.order.dist
            self._dtime = val
            self._vtime = val + delta_seconds
            self._rtime = self._vtime + delta_seconds
            delta_hours = (self._vtime - self.order.time) // 3600
            self._rating = max(10 - delta_hours, 0)
        self._category = self.CATEGORIES[self._rating]

    @property
    def del_time(self) -> int:
        """
        Delivery time. Read-only: automatically updated from dep_time.
        """
        return self._vtime

    @property
    def ret_time(self) -> int:
        """
        Return time. Read-only: automatically updated from dep_time.
        """
        return self._rtime

    @property
    def rating(self) -> int:
        """
        Estimated customer rating (integer 0 - 10). Read-only: automatically updated from dep_time.
        """
        return self._rating

    @property
    def category(self) -> int:
        """
        Estimated customer rating category (promoter, neutral, detractor)
        as (+1, 0, -1) respectively. Read-only: automatically updated from dep_time.
        """
        return self._category

    def __eq__(self, other: "Delivery") -> bool:
        """
        Two deliveries are equal iff their Orders are equal.
        """
        return self.order == other.order

    def __repr__(self) -> str:
        """
        Debug utility method: print internal representation of the Delivery as a string.
        String can be executed to reconstitute the Delivery object, including its Order.
        """
        ostr = repr(self.order)
        return f"Delivery({ostr}, departure={self._dtime})"

    def __str__(self) -> str:
        """
        Return a string representation of the Delivery suitable for printing.
        """
        if self.dep_time is not None:
            departure_timestamp = Order.timestr(self.dep_time)
        else:
            departure_timestamp = "undelivered"
        return f"{self.order.id} {departure_timestamp}"
