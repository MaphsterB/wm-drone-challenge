"""
order.py - Module containing Order class.
"""


import datetime as dt
import re

from typing import Tuple


class Order:
    """
    Utility class representing information about an Order.
    Contains the order ID (a unique string among all Orders),
    coordinates (a pair of strings specifying NS/EW distances),
    and timestamp (time, as integer seconds since midnight).

    Orders are immutable. Do not make changes to an Order object
    after it has been constructed.
    """

    # Class config vars: regex patterns for order string parsing
    ID_PATTERN = r"(WM\d{3})"
    DIRECTION_PATTERN = r"([NS]?\d+)([EW]?\d+)"
    TIMESTAMP_PATTERN = r"(\d\d:\d\d:\d\d)"
    REGEXP = re.compile(r"\s".join((ID_PATTERN, DIRECTION_PATTERN, TIMESTAMP_PATTERN)))
    TIMESTAMP_FORMAT = "%H:%M:%S"

    def __init__(self, id_: str, coords: Tuple[str, str], time: int) -> None:
        self.id = id_
        self.coords = coords
        self.time = time
        self.dist = sum(int(x[1:]) for x in coords)
        if self.dist == 0:
            raise ValueError("Order distance must be non-zero.")

    @staticmethod
    def timestr(seconds: int, fmt: str = None) -> str:
        """
        Helper: Convert integer seconds-since-midnight to a time string in
        TIMESTAMP_FORMAT (or some other specified format).
        """
        fmt = Order.TIMESTAMP_FORMAT if fmt is None else fmt
        return dt.time(
            hour=seconds // 3600,
            minute=(seconds % 3600) // 60,
            second=seconds % 60,
        ).strftime(fmt)

    @classmethod
    def fromstr(cls, order_str: str) -> "Order":
        """
        Create an Order from a string, using class-level regex patterns.
        Raises ValueError if string format is incorrect.
        """
        match = re.match(cls.REGEXP, order_str)
        if not match:
            raise ValueError(f"{order_str} does not match pattern {cls.REGEXP}.")
        (id_, ns_dist, ew_dist, timestamp) = match.groups()
        try:
            timestamp = dt.datetime.strptime(timestamp, cls.TIMESTAMP_FORMAT)
        except ValueError:
            raise ValueError(f"{timestamp} does not match format {cls.TIMESTAMP_FORMAT}.")
        coords = (ns_dist, ew_dist)
        time = timestamp.hour * 3600 + timestamp.minute * 60 + timestamp.second
        return cls(id_, coords, time)

    def __eq__(self, other: "Order") -> bool:
        """
        Equality operator overload. Order are equal iff their ID's are equal.
        """
        return self.id == other.id

    def __repr__(self) -> str:
        """
        Debug utility method: print internal representation of the Order as a string.
        String can be executed to reconstitute the Order object.
        """
        return f"Order('{self.id}', {self.coords}, {self.time})"

    def __str__(self) -> str:
        """
        Return string representation of the Order.
        """
        timestamp = self.timestr(self.time)
        return f"{self.id} {self.coords[0]}{self.coords[1]} {timestamp}"
