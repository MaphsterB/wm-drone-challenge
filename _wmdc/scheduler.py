"""
scheduler.py - Module containing Scheduler class.
"""
# pylint: disable=fixme


import itertools as it

from typing import List, Optional, Sequence, Tuple

from .delivery import Delivery, RatingCategory
from .order import Order


class Scheduler:
    """
    Drone delivery scheduling class. Given a list of Orders, it constructs
    the list of Deliveries to schedule. maximize_nps() is the primary action
    method, which re-orders Deliveries to maximize Net Promoter Score (NPS).

    The Scheduler may restrict when the drone operates using its min_departure
    and max_return parameters. min_departure is the minimum allowed departure
    time, max_return is the maximum allowed return time. Both are specified
    as integer seconds since midnight.
    """

    def __init__(
            self,
            orders: Sequence[Order],
            min_departure: int = 0,
            max_return: int = 24 * 3600) -> None:
        assert min_departure < 24 * 3600, "min_departure must be before next midnight."
        assert max_return <= 24 * 3600, "max_return must be at or before next midnight."
        assert max_return > min_departure, "max_return and min_departure must not overlap."
        self.orders = orders
        self.min_departure = min_departure
        self.max_return = max_return
        self.deliveries = [Delivery(o) for o in orders]
        self.set_departures()

    def set_departures(self, start: int = 0, end: Optional[int] = None) -> None:
        """
        Helper: update departure times of all Deliveries; needed after any re-ordering.
        """
        if end is None:
            end = len(self.deliveries)
        min_departure = self.min_departure
        for i in range(start, end):
            # Don't deliver orders where the drone couldn't get back in time.
            if min_departure is None:
                self.deliveries[i].dep_time = None
                continue
            dep_time = max(self.deliveries[i].order.time, min_departure)
            deadline = self.max_return - 2 * self.deliveries[i].order.dist
            self.deliveries[i].dep_time = (
                None if dep_time > deadline
                else dep_time)
            if i < len(self.deliveries) - 1:
                min_departure = self.deliveries[i].ret_time

    def get_nps_num(self) -> int:
        """
        Numerator of the NPS computation: the sum of all delivery rating categories.
        """
        return sum(d.category.value for d in self.deliveries)

    def get_nps(self) -> float:
        """
        NPS computation: % promoters (+1 category) - % detractors (-1 category),
        with neutral as the 0 category.
        """
        assert self.deliveries, "Cannot compute NPS of an empty delivery list."
        return 100 * self.get_nps_num() / len(self.deliveries)

    def maximize_nps(self) -> None:
        """
        Re-order deliveries to achieve maximum NPS. NPS is maximized when
        count of promoters, then count of non-detractors, are maximized, which
        is equivalent to maximizing the NPS numerator.

        The only way to improve NPS is to improve the delivery times of non-promoters,
        such that their score improves, without a commensurate decrease in score from
        other orders.

        Repeatedly scan through the list of deliveries. Each time we hit a non-promoter,
        see if there is somewhere earlier we can place it to improve overall NPS. Halt
        when no swaps are made.
        """
        # Early drop-out for zero or one orders.
        if len(self.deliveries) < 2:
            return
        done_swapping = False # type: bool
        while not done_swapping:
            done_swapping = True
            # i is the current delivery being considered
            # j is the current target location being considered
            for (i, delivery) in enumerate(self.deliveries):
                if delivery.category == RatingCategory.Promoter:
                    continue
                # TODO performance enhancement: detect "unimprovable" orders
                # and skip them (known neutral/detractor)
                # Start at 0 to ensure only positive moves are made.
                cur_nps = self.get_nps_num()
                max_improvement = 0 # type: int
                best_move = None # type: int
                for j in range(i):
                    # Try inserting delivery[i] before delivery[j]
                    self.advance(i, j)
                    delta = self.get_nps_num() - cur_nps
                    self.unadvance(i, j)
                    # Did we see overall improvement in NPS?
                    if delta > max_improvement:
                        # TODO performance optimization: early dropout
                        # if detractor && improvement == 2 or if neutral && improvement == 1
                        max_improvement = delta
                        best_move = j
                if best_move is not None:
                    self.advance(i, best_move)
                    done_swapping = False

    def advance(self, i: int, j: int) -> None:
        """
        Helper: advance the delivery in slot i such that it is before the delivery in slot j.
        (meaning it becomes the delivery in slot j, and all other deliveries get pushed)
        """
        assert i > j >= 0
        tmp = self.deliveries[i]
        del self.deliveries[i]
        self.deliveries.insert(j, tmp)
        # TODO only need to update departures for i onwards
        self.set_departures()

    def unadvance(self, i: int, j: int) -> None:
        """
        Helper: Undo an advance(). (i, j) should be the same as they were in the corresponding
        advance() call.
        """
        assert i > j >= 0
        tmp = self.deliveries[j]
        del self.deliveries[j]
        self.deliveries.insert(i, tmp)
        # TODO only need to update departures for j onwards
        self.set_departures()

    @staticmethod
    def slow_maximize_nps(
            orders: Sequence[Order],
            min_departure: float,
            max_return: float) -> Tuple[List[Delivery], float]:
        """
        Debug helper: Maximize NPS using brute force. Try all permutations of the Delivery
        list, and report back the 2-tuple: (scheduled, final_nps).

        Note: This naiive algorithm operates in O(N*N!) - do not use in production!
            (we must compute O(N) NPS for each of O(N!) permutations)
        """
        def try_permutation(olist: Sequence[Order]) -> Tuple[List[Delivery], float]:
            sched = Scheduler(olist, min_departure=min_departure, max_return=max_return)
            return (sched.deliveries, sched.get_nps())
        permutations = map(try_permutation, it.permutations(orders))
        (scheduled, max_nps) = max(permutations, key=lambda t: t[1])
        return (scheduled, max_nps)
